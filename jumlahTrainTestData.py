import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.model_selection import train_test_split # function for splitting data to train and test sets
import re
from nltk.corpus import stopwords
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize 
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory


# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()

#print(dataset.isnull())

dataset = pd.read_csv('copian.csv', delimiter = ";", quoting = 3)
# Keeping only the neccessary columns

corpus = []
for i in dataset['teks']:
	review = re.sub('[^a-zA-Z]', ' ', i)
	review = review.lower()
	review = review.split()
	review= [stemmer.stem(w) for w in review if w not in set(stopwords.words('indonesian'))]
	review = ' '.join(review)
	corpus.append(review)

label = dataset.iloc[:, 1]

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(corpus, label, test_size = 0.2, random_state = 123)


posTrain = []
negTrain = []
posTest = []
negTest = []

a,b,c,d = 0,0,0,0

for latih in y_train:
	if "pos" in latih:
		a += 1
	elif "neg" in latih:
		b += 1

for uji in y_test:
	if "pos" in uji:
		c += 1
	elif "neg" in uji:
		d += 1

print("pos Train = {}".format(a))
print("neg Train = {}".format(b))
print("pos Test = {}".format(c))
print("neg Test = {}".format(d))