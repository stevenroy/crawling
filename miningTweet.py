import csv
import pandas as pd
import re
import nltk
import string
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize 
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory


# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()

#dataset = pd.read_csv('hasil.csv', delimiter = ";", quoting = 3)

'''
for i in range(6):
	review = re.sub(r'(?:\@|https?\://)\S+', ' ', dataset['tweet'][i])
	review = review.lower()
	review = stemmer.stem(review)
	word_tokens = word_tokenize(review) 
	stop_words = set(stopwords.words('indonesian'))
	corpus = [w for w in word_tokens if w not in stop_words]
'''

corpus = []
dataset = pd.read_csv('hasil.csv', delimiter = ";", quoting = 3)
for x in range(10):
	for row in dataset['tweet']:
		corpus.append(row)
print(corpus)
print("/n")

for x in corpus:
	review = re.sub(r'(?:\@|https?\://)\S+', ' ', x)
	review = review.lower()
	review = stemmer.stem(review)
	word_tokens = word_tokenize(review) 
	stop_words = set(stopwords.words('indonesian'))
	kam = [w for w in word_tokens if w not in stop_words]
	print(kam)