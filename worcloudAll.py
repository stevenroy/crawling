import pandas as pd 
import numpy as np
import re
from nltk.corpus import stopwords
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from PIL import Image

factory = StemmerFactory()
stemmer = factory.create_stemmer()

dataset = pd.read_csv('ulasan.csv', delimiter = ";", quoting = 3)

corpus = []
for i in dataset['teks']:
	review = re.sub('[^a-zA-Z]', ' ', i)
	review = review.lower()
	review = review.split()
	review= [stemmer.stem(w) for w in review if w not in set(stopwords.words('indonesian'))]
	review = ' '.join(review)
	corpus.append(review)

#Join All tokens into one string
text = " ".join(corpus)

#Generate a word cloud image
our_mask = np.array(Image.open('mask-cloud.png'))
cloud = WordCloud(background_color = "white", mask = our_mask, max_words=100, collocations=False).generate(text)

#Plot the WordCloud image 
plt.figure(1,figsize=(12, 12))
plt.imshow(cloud, interpolation='bilinear')
plt.axis("off")
plt.show()