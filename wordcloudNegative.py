import matplotlib.pyplot as plt
import numpy
import pandas as pd
from PIL import Image
from wordcloud import WordCloud
import numpy as np
import csv

negative = open('negativeSentence.csv', 'r')

#Join All tokens into one string
text = " ".join(x.strip('\n') for x in negative)

#Generate a word cloud image
our_mask = np.array(Image.open('mask-cloud.png'))
cloud = WordCloud(background_color = "white", mask = our_mask, max_words=100, collocations=False).generate(text)

#Plot the WordCloud image 
plt.figure(1,figsize=(12, 12))
plt.imshow(cloud, interpolation='bilinear')
plt.axis("off")
plt.show()
