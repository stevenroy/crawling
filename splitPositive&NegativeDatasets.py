import pandas as pd
import re
from nltk.corpus import stopwords
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import csv

factory = StemmerFactory()
stemmer = factory.create_stemmer()

dataset = pd.read_csv('ulasan.csv', delimiter = ";", quoting = 3)

corpus = []
for i in dataset['teks']:
	review = re.sub('[^a-zA-Z]', ' ', i)
	review = review.lower()
	review = review.split()
	review= [w for w in review if w not in set(stopwords.words('indonesian'))]
	review = ' '.join(review)
	corpus.append(review)

label = dataset.iloc[:, 1]

for x,y in zip(corpus, label):
	if "neg" in y:
		f = open('negativeSentence.csv', 'a')
		f.write(x)
		f.write('\n')
		f.close()
	elif "pos" in y:
		f = open('positiveSentence.csv', 'a')
		f.write(x)
		f.write('\n')
		f.close()
