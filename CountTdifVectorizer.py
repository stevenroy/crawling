import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
#import seaborn as sns
import re

#import pickle 
#import mglearn
#import time


from nltk.tokenize import TweetTokenizer # doesn't split at apostrophes
import nltk
from nltk import Text
from nltk.tokenize import regexp_tokenize
from nltk.tokenize import word_tokenize  
from nltk.tokenize import sent_tokenize 
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.stem import PorterStemmer


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression 
from sklearn.naive_bayes import MultinomialNB
from sklearn.multiclass import OneVsRestClassifier


from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import make_pipeline

dataset = pd.read_csv('hasil.csv', delimiter = ";", quoting = 3)

corpus = []
for i in range(30):
	review = re.sub('http\S+|u([0-9]+)|@([A-Za-z0-9_]+)|#([A-Za-z0-9_]+)|[^\w]|([0-9])', ' ', dataset['tweet'][i])
	review = review.lower()
	review = review.split()
	review = " ".join(review)
	corpus.append(review)



Corpus = [x for x in corpus if x]

file1 = open("kambing.txt").read().split() 


count_vec = CountVectorizer(stop_words=frozenset(file1), analyzer='word', 
                            ngram_range=(1, 1), max_df=1.0, min_df=1, max_features=None)

# Transforms the data into a bag of words
count_train = count_vec.fit(Corpus)
bag_of_words = count_vec.transform(Corpus)

# Print the first 10 features of the count_vec
#print("Every feature:\n{}".format(count_vec.get_feature_names()))
