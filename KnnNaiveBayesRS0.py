import pandas as pd
import re
import nltk
from nltk.corpus import stopwords
from nltk.classify import SklearnClassifier
from sklearn import preprocessing
from nltk.corpus import stopwords
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from sklearn.metrics import confusion_matrix
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn import metrics

# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()

#importing the dataset
dataset = pd.read_csv('ulasan.csv', delimiter = ";", quoting = 3)

corpus = []

#cleaning & preprocessing dataset
for i in dataset['teks']:
	review = re.sub('[^a-zA-Z]', ' ', i)
	review = review.lower()
	review = review.split()
	review= [stemmer.stem(w) for w in review if w not in set(stopwords.words('indonesian'))]
	review = ' '.join(review)
	corpus.append(review)

#Vectorization of data to create the bag of words model
cv = CountVectorizer()
X = cv.fit_transform(corpus).toarray()

#Encoding sentimen
label = dataset.iloc[:, 1]
le = preprocessing.LabelEncoder()
y = le.fit_transform(label)


#Splitting the dataset into the training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)
	
print("Algoritma KNN dengan random_state(0)\n")

#Classification algorithm K-Nearest Neighbor
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors = 2)

#Fitting KNN to the test results
knn.fit(X_train, y_train)

#Predict the response for test sets
y_pred = knn.predict(X_test)

#Making the confusion matrix
cm = confusion_matrix(y_test, y_pred)
print ("Confusion Matrix:\n",cm)

#Accuracy, Precission, Recall
knnAccuracy = metrics.accuracy_score(y_test, y_pred)
knnPrecission = metrics.precision_score(y_test,y_pred)
knnRecall = metrics.recall_score(y_test,y_pred)
print("\nAccuracy KNN: ", round(knnAccuracy*100,2),"%")
print("Precission KNN: ", round(knnPrecission*100,2),"%")
print("Recall KNN: ", round(knnRecall*100,2),"%")


print("\nAlgoritma Multinomial Naive Bayes dengan random_state(0)")

#Classification algorithm Multinomial Naive Bayes
from sklearn.naive_bayes import MultinomialNB
naivebayes = MultinomialNB()

#Fitting Naive Bayes to the test results
naivebayes.fit(X_train, y_train)

#Predict the response for test sets
y_pred = naivebayes.predict(X_test)

#Making the confusion matrix
cm = confusion_matrix(y_test, y_pred)
print ("\nConfusion Matrix:\n",cm)

#Accuracy, Precission, Recall
naivebayesAccuracy = metrics.accuracy_score(y_test, y_pred)
naivebayesPrecission = metrics.precision_score(y_test,y_pred)
naivebayesRecall = metrics.recall_score(y_test,y_pred)
print("\nAccuracy Naive Bayes: ", round(naivebayesAccuracy*100,2),"%")
print("Precission Naive Bayes: ", round(naivebayesPrecission*100,2),"%")
print("Recall Naive Bayes: ", round(naivebayesRecall*100,2),"%")
print("\n")