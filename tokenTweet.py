import csv
import pandas as pd
import re
import nltk
import string
import numpy as np
from nltk.corpus import stopwords
from nltk import FreqDist
from nltk.tokenize import word_tokenize 
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

factory = StemmerFactory()
stemmer = factory.create_stemmer()

dataset = pd.read_csv('ulasan.csv', delimiter = ";", quoting = 3)

corpus = []
for i in dataset['teks']:
	review = re.sub('[^a-zA-Z]', ' ', i)
	review = review.lower()
	review = review.split()
	review= [w for w in review if w not in set(stopwords.words('indonesian'))]
	review = ' '.join(review)
	corpus.append(review)

text = " ".join(x for x in corpus)

#Get the frequency distribution for each word
tokens = nltk.word_tokenize(text)
fdist=FreqDist(tokens)

#Get a list of the 50 most frequently occurring types in the text
freq_dist = fdist.most_common(30)
print(freq_dist)
