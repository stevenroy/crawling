import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.model_selection import train_test_split # function for splitting data to train and test sets
import re
from nltk.corpus import stopwords
from nltk.classify import SklearnClassifier
from sklearn import preprocessing
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize 
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory


# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()

#print(dataset.isnull())

dataset = pd.read_csv('ulasan.csv', delimiter = ";", quoting = 3)
# Keeping only the neccessary columns

corpus = []
for i in dataset['teks']:
	review = re.sub('[^a-zA-Z]', ' ', i)
	review = review.lower()
	review = review.split()
	review= [stemmer.stem(w) for w in review if w not in set(stopwords.words('indonesian'))]
	review = ' '.join(review)
	corpus.append(review)

#le = preprocessing.LabelEncoder()
from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer()
X = cv.fit_transform(corpus).toarray()
label = dataset.iloc[:, 1]
le = preprocessing.LabelEncoder()
y = le.fit_transform(label)

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 42)

print(X_train.shape)
print(y_train.shape)
print(X_test.shape)
print(y_test.shape)
print("\n")

from sklearn.naive_bayes import MultinomialNB
classifier = MultinomialNB()
classifier.fit(X_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print ("Confusion Matrix:\n",cm)

# Accuracy, Precision and Recall
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import classification_report

score1 = accuracy_score(y_test,y_pred)
score2 = precision_score(y_test,y_pred)
score3= recall_score(y_test,y_pred)
print("\n")
print("Accuracy is ",round(score1*100,2),"%")
print("Precision is ",round(score2,2))
print("Recall is ",round(score3,2))
print(classification_report(y_test , y_pred))

from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors = 2)

#Train the model using the training sets
knn.fit(X_train, y_train)

#Predict the response for test dataset
y_pred = knn.predict(X_test)

cm = confusion_matrix(y_test, y_pred)
print ("Confusion Matrix:\n",cm)

score1 = accuracy_score(y_test,y_pred)
score2 = precision_score(y_test,y_pred)
score3= recall_score(y_test,y_pred)
print("\n")
print("Accuracy is ",round(score1*100,2),"%")
print("Precision is ",round(score2,2))
print("Recall is ",round(score3,2))
print(classification_report(y_test , y_pred))