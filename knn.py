import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# function for splitting data to train and test sets
import re
from nltk.corpus import stopwords
from nltk.classify import SklearnClassifier
from sklearn import preprocessing
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize 
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from sklearn.metrics import confusion_matrix
import nltk
from nltk import FreqDist
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from PIL import Image

# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()

dataset = pd.read_csv('copian.csv', delimiter = ";", quoting = 3)
#print(dataset.isnull())

# Keeping only the neccessary columns

corpus = []
for i in dataset['teks']:
	review = re.sub('[^a-zA-Z]', ' ', i)
	review = review.lower()
	review = review.split()
	review= [stemmer.stem(w) for w in review if w not in set(stopwords.words('indonesian'))]
	review = ' '.join(review)
	corpus.append(review)
'''
text = " ".join(corpus)
our_mask = np.array(Image.open('mask-cloud.png'))

cloud = WordCloud(background_color = "white", max_words=250).generate(text)

plt.imshow(cloud)
plt.axis("off")
plt.show()

text = " ".join(corpus)
tokens = nltk.word_tokenize(text)
fdist=FreqDist(tokens)
for word, frequency in fdist.most_common(250):
         print(u'{} : {}'.format(word, frequency))

'''

#le = preprocessing.LabelEncoder()
from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer()
X = cv.fit_transform(corpus).toarray()
label = dataset.iloc[:, 1]
le = preprocessing.LabelEncoder()
y = le.fit_transform(label)

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state = 109)

from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors=5)

#Train the model using the training sets
knn.fit(X_train, y_train)

#Predict the response for test dataset
y_pred = knn.predict(X_test)

from sklearn import metrics
knnAccuracy = metrics.accuracy_score(y_test, y_pred)
# Model Accuracy, how often is the classifier correct?
print("\nAccuracy KNN: ", round(knnAccuracy*100,2))
cm = confusion_matrix(y_test, y_pred)
print ("Confusion Matrix:\n",cm)
print(metrics.classification_report(y_test , y_pred))


from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier()

# Train Decision Tree Classifer
clf = clf.fit(X_train,y_train)

#Predict the response for test dataset
y_pred = clf.predict(X_test)
decisiontree = metrics.accuracy_score(y_test, y_pred)
print("Accuracy Decision Tree:", round(decisiontree*100,2))
