import matplotlib.pyplot as plt
import pandas as pd 
import numpy as np
import re
from nltk import FreqDist
import nltk
from nltk.corpus import stopwords
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from wordcloud import WordCloud

from PIL import Image

positive = open('positiveSentence.csv', 'r')

#Join All tokens into one string
text = " ".join(x.strip('\n') for x in positive)

#Get the frequency distribution for each word
tokens = nltk.word_tokenize(text)
fdist=FreqDist(tokens)

#Get a list of the 30 most frequently occurring types in the text
fdist.plt(30)