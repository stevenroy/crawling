import re
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()


example_sent = "@FirstMediaCares gangguan lagi perbaikan jaringan lagi... Gilaaaak.. #2019GantiProvider"

#Case Folding
lowerText = example_sent.lower()

#Stemming Text
stemmerText = stemmer.stem(lowerText)

stop_words = set(stopwords.words('indonesian'))
  
URLless_string = re.sub(r'(?:\@|https?\://)\S+|[^\w\s]',  '', stemmerText)
#URLless_string= re.sub(r'[^\w\s]',' ',URLless_string)

word_tokens = word_tokenize(URLless_string) 
corpus = []
filtered_sentence = [w for w in word_tokens if not w in stop_words] 
ayam = " ".join(filtered_sentence)
corpus.append(ayam)
'''
for w in word_tokens: 
    if w not in stop_words: 
        filtered_sentence.append(w) 
'''

print(filtered_sentence)
print(ayam)
print(corpus)
print(word_tokens)
print(URLless_string)
