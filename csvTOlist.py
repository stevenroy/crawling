'''
my_list = ['apple', 'banana', 'grapes', 'pear', '']
for c, value in enumerate(my_list, 1):
    print(c, value)

ayam = ["abang", "adek", "papa", "mama"]

for c, value in enumerate(corpus, 1):
    print(c, value)

for a,b in zip(my_list, ayam):
	print(a,b)
'''

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# function for splitting data to train and test sets
import re
from nltk.corpus import stopwords
from nltk.classify import SklearnClassifier
from sklearn import preprocessing
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize 
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from sklearn.metrics import confusion_matrix
import nltk
from nltk import FreqDist
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from PIL import Image

# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()

dataset = pd.read_csv('copian.csv', delimiter = ";", quoting = 3)
#print(dataset.isnull())

# Keeping only the neccessary columns

corpus = []
for i in dataset['teks']:
	review = re.sub('[^a-zA-Z]', ' ', i)
	review = review.lower()
	review = review.split()
	review= [stemmer.stem(w) for w in review if w not in set(stopwords.words('indonesian'))]
	review = ' '.join(review)
	corpus.append(review)



from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer()
X = cv.fit_transform(corpus).toarray()
#le = preprocessing.LabelEncoder()
label = dataset.iloc[:, 1]
le = preprocessing.LabelEncoder()
y = le.fit_transform(label)

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state = 109)


negTrain = 0
posTrain = 0
negTest = 0
posTest = 0

for x in y_train:
	if x == 0:
		negTrain += 1
	elif x == 1:
		posTrain += 1

print("Data Latih Negatif: {}".format(negTrain))
print("Data Latih Positif: {}".format(posTrain))

for x in y_test:
	if x == 0:
		negTest += 1
	elif x == 1:
		posTest += 1

print("Data Latih Negatif: {}".format(negTest))
print("Data Latih Positif: {}".format(posTest))